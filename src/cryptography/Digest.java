package cryptography;

/*
 * Authors: Former Group "Storm", Lech Jankowski SD1 Y3
 */

/**
 *
 * @author Lech Jankowski
 */

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;



public class Digest
{
    
    public Digest()
    {}
    
    public static String bytesToHex(byte[] b) {
       char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                          '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
       StringBuffer buf = new StringBuffer();
       for (int j=0; j<b.length; j++) {
          buf.append(hexDigit[(b[j] >>> 4) & 0x0f]);
          buf.append(hexDigit[b[j] & 0x0f]);
       }
       return buf.toString();
    }        
    
	// to use later
    public static String generateHexSalt() {
        Random sr = new SecureRandom();
        byte[] salt = new byte[20];
        sr.nextBytes(salt);
        return bytesToHex(salt);
    }    

    public static String stringToHashSHA1(String input) {
         String hexSHA1 = null;
         try {      
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(input.getBytes()); 
            byte[] output = md.digest();
            hexSHA1 = bytesToHex(output);                
         } catch (Exception e) {
            System.out.println("Exception: "+e);
         }
         return hexSHA1;
    }
}
