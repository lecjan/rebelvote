package hashtable;


import hashtable.NodeDblSided;
import hashtable.HashTableSlot;
import uservote.Vote;

/*
 * Authors: Former Group "Storm", Sean Enright, Lech Jankowski SD1 Y3
 */

/**
 *
 * @author Lech Jankowski (fully redesigned)
 */

public class HashTable {

    private HashTableSlot[] slots;
    private String name;
    private int size;
    
    public HashTable(String name, String[] categories) {
        System.out.println("name:"+name);
        this.size = categories.length;
        this.name = name;
        slots = new HashTableSlot[size];
        for (int i = 0; i < size; i++) {
            slots[i] = new HashTableSlot(i,categories[i]);
            System.out.println("i:"+i+", cat:"+categories[i]);
        }
    }      
    
    public HashTableSlot[] getSlots()
    {
        return slots;
    }

    public void setSlots(HashTableSlot[] slots)
    {
        this.slots = slots;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }    

    @Override
    public String toString()
    {
        String s="";
        /*
        for (HashTableSlot sl: slots) {
            s=s+sl.toString()+"\n";
        }*/
        for (int i=0;i<slots.length;i++)
        {
            //System.out.println("i:"+i+":");
            String slotString = slots[i].toString();
            //System.out.println(slots[i].toString());
            s=s+slotString+"\n";
        }
        return "HashTable{" + "slots=" + s + ", name=" + name + ", size=" + size + '}';
    }    
    
    public int findSlotByHash(int hash)
    {
        if (slots.length != 0) {
            for (int i=0; i<slots.length; i++)
            {
                //System.out.println("slots[i="+i+"]");
                //System.out.println("slots[i].getHash()="+slots[i].getHash());     
                //System.out.println("Hash="+hash);                 
                if (slots[i].getHash() == hash) {
                    //System.out.println(i+", slothash:"+slots[i].getHash()+", inputhash:"+hash);
                    return slots[i].getIndex();
                } 
            } 
            return -1;
        } else return -1;
    }
    
    
    public int addSlot(int newHash, String newCategory)
    {
        //System.out.println("1.ADD SLOT slots.len="+slots.length+","+newCategory);  
        
        // creating temp slot with index + 1 and new category
        HashTableSlot tmpSlot = new HashTableSlot(slots.length,newCategory);
        //System.out.println("addSlot:"+tmpSlot.toString()); 
        // expanding slots array
        HashTableSlot [] slotsExpanded = new HashTableSlot[slots.length+1];
        // setting new=last slot to temp one
        for (int i=0; i<slots.length; i++)
        {
            slotsExpanded[i]=slots[i];
        }
        slotsExpanded[slotsExpanded.length-1]=tmpSlot;  
        slots = slotsExpanded;
        
        //System.out.println("2.ADD SLOT slots.len="+slots.length+","+newCategory+":"+slots[slots.length-1].toString());    
        
        // increase hashtable size
        this.setSize(slots.length);
        return slots.length-1;  // returns index of a slot
    }
    
    public int addSlotByVoterName(Vote addVote)
    {
        //System.out.println("ADD SLOT BY VOTER NAME"); 
        // adding new slot to hashtable for name + surname as a category
        // String newCategory = addVote.getSurname() + "," + addVote.getName(); for full individual name
        String newCategory = addVote.getSurname().substring(0, 1);        
        int newHash = newCategory.hashCode();
        return addSlot(newHash,newCategory); // returns index of a slot
    }    
    
    public boolean put(int hash, Vote addVote) {
        
        //System.out.println("hash:"+hash+","+addVote.getParty()+","+addVote.hashParty());
        int slotNumber = findSlotByHash(hash);
        //System.out.println("slotNumber:"+slotNumber);
        if (slotNumber == -1) { // no such a slot add the uknown vote party to a default category slot "NO-VOTE"
            slotNumber = 0;  // adds new slot and category and returns index of new added slot
            //System.out.println("slotNumber 2:"+slotNumber+":"+slots[slotNumber].toString());
            //System.out.println("before3:"+this.toString());
        } 
        if (slots[slotNumber] != null) { // if slot initiated
            NodeDblSided n = new NodeDblSided(addVote);
            LinkedListDoubleEnded tempList = slots[slotNumber].getVotes(); // gets proper (from valid slot) linkedlist to add to
            tempList.insertInOrder(n); // inserts new link in asc order by age into temp linkedlist
            slots[slotNumber].setVotes(tempList); // replacing slot's linkedlist with a temp one
            slots[slotNumber].setParameter(tempList.calculateAvgAge()); // setting column parameter to result of calculate average age method.
            return true;
        }
        return false;
    }    
    
    
	// version for adding a vote into empty hashtable (no slots defined and no categories)
    public boolean putInitial(int hash, Vote addVote) {
        
        //System.out.println("hash:"+hash+","+addVote.getParty()+","+addVote.hashParty());
        //System.out.println("before2:"+this.toString());
        int slotNumber = findSlotByHash(hash);
        //System.out.println("slotNumber 1:"+slotNumber);
        if (slotNumber == -1) { // no such a slot add new one
            slotNumber = addSlotByVoterName(addVote);  // adds new slot and category and returns index of new added slot
            //System.out.println("slotNumber 2:"+slotNumber+":"+slots[slotNumber].toString());
            //System.out.println("before3:"+this.toString());
        } 
        
        // dbl check
        if (slotNumber == -1) { // no such a slot
            return false;
        }         
        // finally adding to proper slot's linkedlist
        if (slots[slotNumber] != null) { // if slot initiated
            NodeDblSided n = new NodeDblSided(addVote);
            LinkedListDoubleEnded tempList = slots[slotNumber].getVotes();
            tempList.insertInOrder(n);
            slots[slotNumber].setVotes(tempList);
            slots[slotNumber].setParameter(tempList.calculateAvgAge()); // setting column parameter to result of calculate average age method.            
            //System.out.println("slotNumber 3:"+slotNumber+":"+slots[slotNumber].toString());
            //System.out.println("before4:"+this.toString());
            return true;
        }
        return false;
    }    
    
    public Vote findVoteForFullName(String fullName)
    {
        Vote v  = null;
        int initialHash = fullName.substring(0, 1).hashCode();
        for (int a=0;a<size;a++)
        {
            if (slots[a].getHash() == initialHash)
            {
                System.out.println("slot:"+a+", initialHash:"+initialHash+" ,fname.hash:"+fullName.hashCode());
                LinkedListDoubleEnded llde = slots[a].getVotes();
                NodeDblSided n = llde.findNodeByHashCode(fullName.hashCode());
                v = n.getContentObject();
            }
        }
        return v;
    }
    
   
    
    /*
    *               TO DO !!! 
    * 
    * getAverageAgeForCategory(int hash) // hash gives a slot reference
    * getListOfVoters(int hash) // hash gives a slot reference
    * sortHashtableBy: hashtable slots[] by index (no sorted), by category asc/desc
	* getListOf
    */   



}
