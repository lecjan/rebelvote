package hashtable;

/*
 * Authors: Former Group "Storm", Lech Jankowski SD1 Y3
 */

/**
 *
 * @author Lech Jankowski
 */

public class HashTableSlot {
    
    private int index;
    private int hash;
    private LinkedListDoubleEnded votes;
    private String category; 
    private long parameter;
    

    public HashTableSlot(int index, String category) {
        this.index = index;
        this.hash = category.hashCode();
        this.votes = new LinkedListDoubleEnded();
        this.category = category;        
    }
    
    public HashTableSlot(int index, int hash, LinkedListDoubleEnded votesGroup, String category) {
        this.index = index;
        this.hash = category.hashCode();
        this.votes = votesGroup;
        this.category = category;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }
    
    public LinkedListDoubleEnded getVotes() {
        return votes;
    }

    public void setVotes(LinkedListDoubleEnded votes) {
        this.votes = votes;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public void setParameter(long parameter)
    {
        this.parameter = parameter;
    }    
    
    public long getParameter()
    {
        return parameter;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.hash;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HashTableSlot other = (HashTableSlot) obj;
        if (this.hash != other.hash) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "\n\tSlot{" + "index=" + index + ", hash=" + hash + ", average age=" + parameter + " => " + votes.getSize() + " voters=" + votes + ", category=" + category + '}';
    }


    
    
    
    
}
