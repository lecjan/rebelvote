package hashtable;


import hashtable.NodeDblSided;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LinkedListDoubleEnded {

    //First define what double ended list consists of.
    NodeDblSided firstLink;
    NodeDblSided lastLink;
    long size;
    long parameter;

    public LinkedListDoubleEnded() {
        firstLink = null;
        lastLink = null;
        size = 0;        
        parameter = 0;
    }

    public LinkedListDoubleEnded(NodeDblSided firstLink, NodeDblSided lastLink) {
        this.firstLink = firstLink;
        this.lastLink = lastLink;        
        size = 0;        
        parameter = 0;
    }

    public long getSize()
    {
        return size;
    }    

    public long getParameter()
    {
        return (long) calculateAvgAge();
    }


    public NodeDblSided findNodeByHashCode(int hc) {
        NodeDblSided current = firstLink;
        NodeDblSided prev = firstLink;
        NodeDblSided tmpN = null;
        while (current.next != null) {
           System.out.println("cur:"+current.hashCode());  
            if (current.hashCode() == hc) {
                tmpN =  current;
            } 
            prev = current;
            current = current.next;
        }
        if (current.hashCode() == hc) {
            tmpN =  current;
        }  
        System.out.println("cur:"+current.hashCode());  
        return tmpN;
    }       
    
    public short calculateAvgAge() {
        long totalCumulativeAge = 0;

        NodeDblSided current = firstLink;
        NodeDblSided prev = firstLink;

        while (current.next != null) {
            totalCumulativeAge += current.getContentObject().getAge();
            prev = current;
            current = current.next;
        }
        totalCumulativeAge += current.getContentObject().getAge();
        return (short)(totalCumulativeAge/size);
    }    
    
    public List<NodeDblSided> getAllNodesAsList() {
        List<NodeDblSided> l = new ArrayList<NodeDblSided>();
        NodeDblSided current = firstLink;
        NodeDblSided prev = firstLink;        
        while (current.next != null) {
            l.add(current);            
            prev = current;
            current = current.next;
        }
        //Do this again here so that it'll get the last item in the list.
        l.add(current);  
        return l;
    }    
    
    public void print() {
        NodeDblSided current = firstLink;
        NodeDblSided prev = firstLink;
        long pos = 0;
        
        while (current.next != null) {
            pos++;
            System.out.println(pos + current.toString());
            prev = current;
            current = current.next;
        }
        //Do this again here so that it'll print the last item in the list.
        pos++;
        System.out.println(pos + current.toString());
    }

    public String prepString() {
        String s = "";
        long pos = 0;
        
        NodeDblSided current = firstLink;
        NodeDblSided prev = firstLink;

        
        if (current != null) {        
            while (current.next != null) {
                pos++;
                s = s + pos + current.toString() + "\n";
                prev = current;
                current = current.next;
            }
            //Do this again here so that it'll print the last item in the list.
            pos++;
            s = s + pos + current.toString() + "\n";            
        }

        return s;
    }
    
    public void insertInOrder(NodeDblSided newNode) {
		NodeDblSided previous = null;
		NodeDblSided current = firstLink;
		while (current != null && current.compareTo(newNode) < 0) {
			previous = current;
			current = current.next;
		}
		if (previous == null) {
			firstLink = newNode;
		} else {
			previous.next = newNode;
		}
		newNode.next = current;
                size++;
    }

    @Override
    public String toString()
    {
        return "{\n" + prepString() + "}";
    }

}
