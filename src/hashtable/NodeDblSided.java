package hashtable;


import uservote.Vote;
import java.util.Objects;

/*
 * Authors: Former Group "Storm", Sean Enright, Lech Jankowski SD1 Y3
 */

/**
 *
 * @author Lech Jankowski (fixed, amended fields, new toString(), separating dblLink from fixed Vote Object and replacing with Object type)
 */

/**
 *
 * @author Sean Enright D00135791
 */
  
public class NodeDblSided implements Comparable<NodeDblSided>{

    private Vote contentObject;

    public NodeDblSided next;
    public NodeDblSided prev; 

    public NodeDblSided(Vote v) {
        this.contentObject = v;
    }

    public Vote getContentObject()
    {
        return contentObject;
    }

    public void setContentObject(Vote v)
    {
        this.contentObject = v;
    }
    
    public NodeDblSided getNext() {
        return next;
    }

    public void setNext(NodeDblSided next) {
        this.next = next;
    }

    public NodeDblSided getPrev() {
        return prev;
    }

    public void setPrevious(NodeDblSided prev) {
        this.prev = prev;
    }
    
    @Override
    public int hashCode()
    {
        int hash = Objects.hashCode(this.contentObject);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final NodeDblSided other = (NodeDblSided) obj;
        if (!Objects.equals(this.contentObject, other.contentObject))
        {
            return false;
        }
        return true;
    }
       

    @Override
    public String toString()
    {
        return "\t\t\t\t\t" + contentObject + "";
    }

    @Override
    public int compareTo(NodeDblSided n)
    {
        return this.contentObject.compareTo(n.getContentObject());
    }
    
    

}
