/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rebelvote;

import hashtable.HashTable;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import static rebelvote.RebelVote.generateResultTables;
import static rebelvote.RebelVote.hashAgeGroups;
import static rebelvote.RebelVote.hashEntryVotes;
import static rebelvote.RebelVote.hashParties;
import static rebelvote.RebelVote.importVotes;

/**
 *
 * @author pcworld
 */
public class FXMLDocumentController implements Initializable
{
    
    @FXML
    private Label labelAlert;
    
    @FXML
    private Label labelSelected;
    
    @FXML
    private Button redparty;
        
    @FXML
    private Button orangeparty;
    
    @FXML
    private Button greenyparty;
    
    @FXML
    private Button socialparty;
    
    @FXML
    private Button blackparty;    
    
    @FXML
    private Button ButtonRegister;
    
    @FXML
    private Button ButtonSubmit; 
    
    @FXML
    private TextArea output; 
    
    @FXML
    private BarChart chart1;     
    
    @FXML
    private void handleButtonActionRed(ActionEvent event)
    {
        System.out.println("You clicked me!");
        labelSelected.setText(redparty.getText());
    }
    
    @FXML
    private void handleButtonActionOrange(ActionEvent event)
    {
        System.out.println("You clicked me!");
        labelSelected.setText(orangeparty.getText());
    }
    
    @FXML
    private void handleButtonActionBlack(ActionEvent event)
    {
        System.out.println("You clicked me!");
        labelSelected.setText(blackparty.getText());
    }   
    
    @FXML
    private void handleButtonActionGreeny(ActionEvent event)
    {
        System.out.println("You clicked me!");
        labelSelected.setText(greenyparty.getText());
    }    
    
    @FXML
    private void handleButtonActionSocial(ActionEvent event)
    {
        System.out.println("You clicked me!");
        labelSelected.setText(socialparty.getText());
    }    
    
    @FXML
    private void handleButtonRegisterAction(ActionEvent event)
    {
        System.out.println("You clicked register!");
        labelAlert.setText("You clicked register!");
    }    
    
    @FXML
    private void handleButtonSubmitAction(ActionEvent event)
    {
        System.out.println("You clicked submit!");
        labelSelected.setText("You clicked submit!");
    }        
    
    @FXML
    private void handleButtonImportAction(ActionEvent event)
    {
        System.out.println("You clicked import!");
        // loading votes from a file voters.txt
        importVotes("voters.txt",hashEntryVotes);
    }         
    
    @FXML
    private void handleButtonResultsAction(ActionEvent event)
    {
        System.out.println("You clicked Generate Results!");
        // loading votes from a file voters.txt
        generateResultTables(hashEntryVotes);
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("2017"); 
        series1.getData().add(new XYChart.Data("Red", 56));
        series1.getData().add(new XYChart.Data("Orange", 120));
        series1.getData().add(new XYChart.Data("Black", 34));
        series1.getData().add(new XYChart.Data("Green", 234));
        series1.getData().add(new XYChart.Data("Social", 22));
        chart1.getData().addAll(series1);
    }          
    
    @FXML
    private void handleButtonResultsPartyAction(ActionEvent event)
    {
        System.out.println("You clicked Party Results!");
        // loading votes from a file voters.txt
        System.out.println(hashParties.toString());
        output.setText(hashParties.toString());

    }     
    
    @FXML
    private void handleButtonResultsAgeGroupAction(ActionEvent event)
    {
        System.out.println("You clicked Age Groups Results!");
        // loading votes from a file voters.txt
        System.out.println(hashAgeGroups.toString());
        output.setText(hashAgeGroups.toString());

    }     
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
    }    
    
}
