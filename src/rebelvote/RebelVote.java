/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rebelvote;

import hashtable.HashTable;
import hashtable.NodeDblSided;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import uservote.Vote;

/**
 *
 * @author pcworld
 */
public class RebelVote extends Application
{
    
    public static HashTable hashEntryVotes;
    public static HashTable hashParties;
    public static HashTable hashAgeGroups;     
    
    @Override
    public void start(Stage stage) throws Exception
    {
           // temporary objects & variables
        List<Vote> votesLoadList = new ArrayList<Vote>();        // to store votes on load from the file
        
        String[] htCategories0 = new String[0];
        String htName0 = "Entry Voters Data";
        hashEntryVotes = new HashTable(htName0, htCategories0);

        
        String[] htCategories1 = {"NO-VOTE","RED PARTY","ORANGE PARTY","BLACK PARTY","GREEN PARTY","SOCIAL PARTY"};
        String htName1 = "Results by winning Party";
        hashParties = new HashTable(htName1, htCategories1);
        
        String[] htCategories2 = {"NULL","0-24","25-49","50-74","75+"};
        String htName2 = "Results by Age Groups";
        hashAgeGroups = new HashTable(htName2, htCategories2);          
        
        
        // loading votes from a file voters.txt
        importVotes("voters.txt",hashEntryVotes);
        
        generateResultTables(hashEntryVotes);
        
        System.out.println(hashEntryVotes.toString());        
        System.out.println(hashParties.toString());
        System.out.println(hashAgeGroups.toString());  
        
        // test to find CAROLYN,JOHNSON in hasEntryVotes
        Vote found = hashEntryVotes.findVoteForFullName("JOHNSON,CAROLYN");
        if (found != null)
        {
            System.out.println(found.toString()); 
        } else {
            System.out.println("NOT FOUND");
        }          
        
        
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    
     public static void importVotes(String fileName, HashTable htToFill)
    {
        HashTable tmpHT = htToFill;
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(RebelVote.class.getName()).log(Level.SEVERE, null, ex);
        }
         try {
             while (true) {
                 String line = br.readLine(); // read next line

                 //System.out.println("line:"+line);
                 if (line != null) {

                     // tokenising: assuming text file is correct nnn,nnn,999,nnnnnnnn; if extra <space> found delete it
                     String[] lineArr = line.split(",");
                     String fName = lineArr[0].toUpperCase();
                     String lName = lineArr[1].toUpperCase();
                     int age = Integer.parseInt(lineArr[2].replace(" ", ""));
                     String votedFor = lineArr[3].toUpperCase();  
                     System.out.println("Read: "+fName+","+lName+","+age+","+votedFor);

                     // create temp Vote
                     Vote newVote = new Vote(false,fName,lName,age,votedFor); 
                     //Vote newVoteScrambled = new Vote(true,fName,lName,age,votedFor); 

                     // loading votes from plain file to "entry" hashtable
                     //System.out.println("hash entry3:"+newVote.hashNames());                          

                     tmpHT.putInitial(newVote.getHashForNames(),newVote);    // try to put again into new category = surname,name

                 } else {
                     break; // no more lines then quit looping - last one was a null
                 }
             }
         } catch (IOException e) {
             e.printStackTrace();
         } finally {
             {
                 if (br != null) try {
                     br.close(); // closing BufferedReader
                 } catch (IOException ex) {
                     Logger.getLogger(RebelVote.class.getName()).log(Level.SEVERE, null, ex);
                 }
             }
         }    

    }

    public static void generateResultTables(HashTable htSource)
    {
        
        for (int slotNumber=0;slotNumber<htSource.getSize();slotNumber++)
        {
            List<NodeDblSided> votesInSlot = htSource.getSlots()[slotNumber].getVotes().getAllNodesAsList();
            for (NodeDblSided n: votesInSlot)
            {
                Vote newVote = n.getContentObject();
                hashParties.put(newVote.getHashForParty(),newVote.getScrambledVote());
                hashAgeGroups.put(newVote.getHashForAgeGroup(),newVote.getScrambledVote());             
            }
        }
    }   
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {

        launch(args);
        
        
   
        

    }
    
}
