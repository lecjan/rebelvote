package uservote;


import cryptography.Digest;
import java.util.Objects;

/*
 * Authors: Former Group "Storm", Sean Enright, Lech Jankowski SD1 Y3
 */

/**
 *
 * @author Lech Jankowski (fixed & changed constructors, implemented: scrambling names, new hashcode() and equals(), 2 new hash methods, toString)
 */

public class Vote implements Comparable<Vote>
{

    private String name,surname,party;
    private int age;

    public Vote() {
        name = "";
        surname = "";
        age = 0;
        party = "NO-VOTE";
    }

    public Vote(boolean scramble, String name, String surname, int age, String party) 
    {
        if (scramble) 
        {
            this.name = Digest.stringToHashSHA1(name);
            this.surname = Digest.stringToHashSHA1(surname);
        } else {
            this.name = name;
            this.surname = surname;            
        }    
        this.age = age;
        this.party = party;
    }
    
    public Vote getScrambledVote()
    {
        Vote scrambledVote = new Vote();
        scrambledVote.name = Digest.stringToHashSHA1(this.name);
        scrambledVote.surname = Digest.stringToHashSHA1(this.surname);
        scrambledVote.age = this.age;
        scrambledVote.party = this.party;
        return scrambledVote;
    }   
    
    public int getHashForNames()
    {
        String hs = this.surname + "," + this.name;
        return hs.substring(0, 1).hashCode();
    }
    
    public int getHashForParty()
    {
        return this.party.hashCode();
    }
    
    public int getHashForAgeGroup()
    {
        int ageGroupIndex = (int)(this.age/25)+1;
        //System.out.println("hasAge: ageModulus:" + ageModulus);
        switch (ageGroupIndex) {
            case 0: return "NULL".hashCode(); // to prevent unwanted hashCode() when age happens to be negative value, and store somewhere
            case 1: return "0-24".hashCode();
            case 2: return "25-49".hashCode();
            case 3: return "50-74".hashCode();
            default: return "75+".hashCode(); // any other values goes over age 75
        } 
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getParty() {
        return party;
    }

    public void setParty(String party) {
        this.party = party;
    }

    @Override
    public String toString() {
        return "" + name + "," + surname + ", age " + age + ", voted for " + party;
    }    
    
    @Override
    public int hashCode() {
        return (this.getSurname() + "," + this.getName()).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vote other = (Vote) obj;
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int compareTo(Vote v)
    {
        return this.age - v.getAge();        
    }

}
